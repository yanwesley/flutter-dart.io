class UF {
  UF({this.id, this.initials, this.name});

  //método para criar um novo objeto json
  //API IBGE
  factory UF.fromJson(Map<String, dynamic> json) =>
      UF (id: json['id'], initials: json['sigla'], name: json['nome']);

  int id;
  String initials;
  String name;
}
