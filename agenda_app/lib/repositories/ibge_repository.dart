import 'package:agenda_app/models/uf.dart';
import 'package:dio/dio.dart';

class IBGERepository {
  Future<List<UF>> getUFListFromAPI() async {
    const endpoint =
        "https://servicodados.ibge.gov.br/api/v1/localidades/estados";

    try {
      final response = await Dio().get<List>(endpoint);

      final ufList = response.data.map<UF>((j) => UF.fromJson(j)).toList();

      return ufList;
    } on DioError {
      return Future.error("Falha na API");
    }
  }

  getCityFromAPI() {}
}
